#!/usr/bin/python

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import sys
import argparse
import os
import time
import subprocess
import logging
import datetime

RUN_LOG = "run.log"

def setupLogger(outdir):
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] %(message)s\n',
                        datefmt='%m-%d %H:%M',
                        filename=os.path.join(outdir,'run.log'),
                        filemode='w')

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt='%H:%M:%S') )
    logging.getLogger('').addHandler(console)


class Process:
    def __init__(self,command,name):
        self.name = name
        self.log = logging.getLogger(RUN_LOG)
        self.args = [command]
        self.skipRun = False
        self.shellEnabled = False
        self.envVars = {}
        self.stdout_str = ""
        self.stderr_str = ""
    
    def getCmd(self):
        return " ".join(self.args)
    
    def addParameter(self, name,value=None):
        self.args.append(name)
        if value is not None:
            self.args.append( str(value) )

    def addEnvVar(self, name, value):
        self.envVars[name] = value

    def removeParameter(self, name, removeValue=False):
        try:
            idx = self.args.index(name)
        except ValueError:
            return 
        el2 = None
        if idx > -1:
            el1 = self.args[idx]
            if removeValue:
                el2 = self.args[idx + 1]
            
            self.args.remove(el1)
            if removeValue:
                self.args.remove(el2)
    
    def resetParameter(self, name, value):
        self.removeParameter(name,True)
        self.addParameter(name,value)

    def processStdErr(self,stderr_str):
        self.log.debug( "Std err output:\n" + stderr_str )

    def run(self):

        self.log.debug( self.getCmd() )
        t0 = time.time()

        if self.skipRun:
            self.log.debug( "Skipped running the process" )
            return

        sysenv = dict(os.environ)

        for name,value in self.envVars.iteritems():
            sysenv[name] = value

        child_process = subprocess.Popen( self.args, stdin=subprocess.PIPE, env=sysenv,
                                            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                            universal_newlines=True, shell=self.shellEnabled )
        
        self.stdout_str, self.stderr_str = child_process.communicate()
           
        return_code = child_process.returncode

        t1 = time.time()

        if return_code:
            logging.debug( "[stdout]\n%s" % self.stdout_str )
            logging.debug( "[stderr]\n%s" % self.stderr_str )
            raise RuntimeError("Process %s returned non-zero exit code" % self.name )
        
        if self.stderr_str:
            self.processStdErr(self.stderr_str)

        self.log.debug( "Process elapsed running time: %s" % str(datetime.timedelta(seconds=(long(t1-t0)))) )


class Pipe:

    def __init__(self, name):
        self.name = name
        self.items = []
        self.out = ""
        self.cmd = ""
        self.stderr = subprocess.PIPE
        self.skipRun = False

    def addProcess(self, p):
        self.items.append(p)
        if len(self.cmd) > 0:
            self.cmd += " | "
        self.cmd +=  p.getCmd()

    def setPipeOutputFile(self, filePath):
        self.out = filePath

    def run(self):

        self.log = logging.getLogger(RUN_LOG)
        t0 = time.time()

        if len(self.out) > 0:
            self.cmd += " > " + self.out


        self.log.debug(self.cmd)

        if self.skipRun:
            logging.debug( "Skipped running the pipeline" )
            return

        p = None
        outstream = subprocess.PIPE
        if len(self.out) > 0:
            outstream = open(self.out, "w")

        lastIndex = len(self.items) - 1
        for i in range(0,lastIndex + 1):
            proc = self.items[i]
            pin = subprocess.PIPE
            if i > 0:
                pin = p.stdout

            sysenv = dict(os.environ)

            for name,value in proc.envVars.iteritems():
                sysenv[name] = value

            pout = subprocess.PIPE if i != lastIndex else outstream
            newP = subprocess.Popen( proc.args, stdin=pin, stdout=pout, stderr=subprocess.PIPE, env=sysenv)
            if i > 0:
                p.stdout.close()
            p = newP


        stdout_str, stderr_str = p.communicate()

        if len(self.out) > 0:
            outstream.close()

        return_code = p.returncode

        t1 = time.time()

        if return_code:
            logging.debug( stderr_str )
            raise RuntimeError("Pipeline %s returned non-zero exit code" % self.name, stderr_str)

        if stderr_str:
            self.stderr_str = stderr_str
            self.log.debug( "Std err output:\n" + stderr_str )

        self.log.debug( "Pipe elapsed running time: %s" % str(datetime.timedelta(seconds=(long(t1-t0)))) )


class Aligner(Process):
    def __init__(self, refPath, inPath1, inPath2, outPath):
        Process.__init__(self,"razers2","aligner_process")
        self.addParameter("-so","1")
        self.addParameter("-o", outPath)
        self.addParameter(refPath)
        self.addParameter(inPath1)
        self.addParameter(inPath2)

class SnpCaller(Process):
    def __init__(self, refPath, alnPath, outPath):
        Process.__init__(self, "snp_store", "snp_caller")
        self.addParameter("-mc", "1")
        self.addParameter("-mm", "1")
        self.addParameter("-o", outPath)
        self.addParameter("-if", "1")
        self.addParameter(refPath)
        self.addParameter(alnPath)
        


