#!/usr/bin/python

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import sys
import argparse
import time
import library
import logging
import datetime
from library import RUN_LOG
from library import Aligner


# Tutorial: 
# Scientific software development 

# NOTE! This is not a real pipeline, but a demonstation 
# In a real pipeline there are many other factors to include:
# piping data streams, output folder and intermidiate data, etc 

        
def runSnpCallingPipeline( refPath, readsPath1, readsPath2, outputPath ):

    t0 = time.time()
    
    library.setupLogger(".")

    logging.getLogger(RUN_LOG).info("Started pipeline")
    
    #TODO: put your code here
    
    # align reads
    
    # call snps
    
    t1 = time.time()

    logging.getLogger(RUN_LOG).info( "Total elapsed time: %s" % str(datetime.timedelta(seconds=(long(t1-t0)))) )

    logging.getLogger(RUN_LOG).info("Finished pipeline")


if __name__ == "__main__":
    
    descriptionText = "Simple bioinformatics pipeline: align paired-end reads and calculate SNPs."
            
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("-1", dest="reads_1", required="true", help="Path to paired-end reads, upstream mates")
    parser.add_argument("-2", dest="reads_2", required="true", help="Path to paired-end reads, downstream mates")
    parser.add_argument("-r", dest="ref", required="true", help="Path to reference sequence")
    parser.add_argument("-o", dest="out_path", required="true", help="Path to output SNPs")
 
    args = parser.parse_args()                   
       
    try:

        runSnpCallingPipeline(args.ref, args.reads_1, args.reads_2, args.out_path )
   
    except RuntimeError as err:
        
        logging.error( "[PIPELINE ERROR]\n%s\nCheck run.log for details." % err.msg )
        sys.exit(42)
        

    



    


