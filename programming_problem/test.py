import random
import unittest
import os
import sys
import difflib
import library
from library import Process
from library import Aligner

def diffFiles(file1, file2):
    file1lines = open(file1).readlines()
    file2lines = open(file2).readlines()
    
    diff = difflib.unified_diff(file1lines, file2lines)
    
    return list(diff)
           

class TestLibrary(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)
        library.setupLogger(".")

    def test_echo(self):
        
        p = Process("echo", "echo_process")
        p.addParameter("BOO")
        p.shellEnabled = True
        p.run()
        self.assertEqual( p.stdout_str, "BOO\n")
            
    def test_cp(self):
        cp_proc_name = "cp"
        if sys.platform == "win32":
           cp_proc_name = "copy"

        file = open("file", "w")
        for i in range(10):
            file.write( "%d\n" % (i*2) )
        file.close()
        
        p = Process(cp_proc_name, "copy_process")
        p.addParameter("file")
        p.addParameter("file2")
        p.shellEnabled = True
        p.run()

        diffItems = diffFiles("file", "file2")
        sys.stderr.writelines(diffItems)
        
        self.assertFalse(diffItems)

        os.remove("file")
        os.remove("file2")
    
    # Test Aligner

    def test_aligner(self):
        
        p = Aligner("ref.fa","reads_1.fq", "reads_2.fq","result.sam")
        p.run()

        diffItems = diffFiles("result.sam", "expected_aln.sam")
        
        self.assertFalse(diffItems)

        os.remove("result.sam")
        

    # Test SnpCaller     

    # TODO: insert your code here

if __name__ == '__main__':
    unittest.main()
